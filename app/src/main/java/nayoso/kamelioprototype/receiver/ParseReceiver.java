package nayoso.kamelioprototype.receiver;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.parse.ParsePushBroadcastReceiver;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import nayoso.kamelioprototype.R;
import nayoso.kamelioprototype.activity.MainActivity;
import nayoso.kamelioprototype.model.ThemeModel;

/**
 * Created by nikoyuwono on 3/24/15.
 */
public class ParseReceiver extends ParsePushBroadcastReceiver {

    private final String TAG = this.getClass().getName();

    @Override
    protected Notification getNotification(Context context, Intent intent) {

        try {
            if (intent.getExtras() != null) {
                ArrayList<ThemeModel> themeModels = new ArrayList<>();
                JSONObject parseData = new JSONObject(intent.getExtras().getString("com.parse" +
                        ".Data"));
                Log.i("NIKO", "PARSE DATA " + parseData.toString());
                JSONObject aps = parseData.getJSONObject("aps");
                JSONArray articles = parseData.getJSONArray("articles");
                int length = articles.length();
                for (int i = 0; i < length; i++) {
                    JSONObject article = articles.getJSONObject(i);
                    ThemeModel themeModel = new ThemeModel();
                    themeModel.setImageUrl(article.getString("image_url"));
                    themeModel.setTitle(article.getString("title"));
                    themeModel.setWebUrl(article.getString("web_url"));
                    themeModels.add(themeModel);
                }
                String alert = aps.getString("alert");

                Intent homeIntent = new Intent(context, MainActivity.class);
                PendingIntent contentIntent = PendingIntent.getActivity(context, 0, homeIntent,
                        PendingIntent.FLAG_CANCEL_CURRENT);

                RemoteViews customNotification = new RemoteViews(context.getPackageName(),
                        R.layout.custom_notification);
                customNotification.setTextViewText(R.id.firstNotificationText,
                        themeModels.get(0).getTitle());
                customNotification.setTextViewText(R.id.secondNotificationText,
                        themeModels.get(1).getTitle());
                customNotification.setTextViewText(R.id.thirdNotificationText,
                        themeModels.get(2).getTitle());

                Intent firstContentIntent = new Intent(context, CustomNotificationTouchListener
                        .class);
                firstContentIntent.putExtra("url", themeModels.get(0).getWebUrl());
                firstContentIntent.putExtra("title", themeModels.get(0).getTitle());
                PendingIntent pendingFirstContentIntent = PendingIntent.getBroadcast(context, 0,
                        firstContentIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                Intent secondContentIntent = new Intent(context, CustomNotificationTouchListener
                        .class);
                secondContentIntent.putExtra("url", themeModels.get(1).getWebUrl());
                secondContentIntent.putExtra("title", themeModels.get(1).getTitle());
                PendingIntent pendingSecondContentIntent = PendingIntent.getBroadcast(context, 1,
                        secondContentIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                Intent thirdContentIntent = new Intent(context, CustomNotificationTouchListener
                        .class);
                thirdContentIntent.putExtra("url", themeModels.get(2).getWebUrl());
                thirdContentIntent.putExtra("title", themeModels.get(2).getTitle());
                PendingIntent pendingThirdContentIntent = PendingIntent.getBroadcast(context, 2,
                        thirdContentIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                customNotification.setOnClickPendingIntent(R.id
                        .firstNotificationContentContainer, pendingFirstContentIntent);
                customNotification.setOnClickPendingIntent(R.id
                        .secondNotificationContentContainer, pendingSecondContentIntent);
                customNotification.setOnClickPendingIntent(R.id
                        .thirdNotificationContentContainer, pendingThirdContentIntent);

                NotificationCompat.Builder builder = new NotificationCompat.Builder(
                        context);

                builder.setSmallIcon(R.drawable.ic_notif);
                builder.setTicker(alert);
                builder.setContentIntent(contentIntent);
                builder.setAutoCancel(true);
                builder.setDefaults(Notification.DEFAULT_ALL);
                builder.setContent(customNotification);

                Notification notification = builder.build();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    notification.bigContentView = customNotification;
                }
                Picasso.with(context).load(themeModels.get(0).getImageUrl()).into
                        (customNotification, R.id.firstNotificationImage, 1, notification);
                Picasso.with(context).load(themeModels.get(1).getImageUrl()).into
                        (customNotification, R.id.secondNotificationImage, 1, notification);
                Picasso.with(context).load(themeModels.get(2).getImageUrl()).into
                        (customNotification, R.id.thirdNotificationImage, 1, notification);
                NotificationManager notificationManager = (NotificationManager) context
                        .getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(1, builder.build());
                return null;
            }
        } catch (JSONException e) {
            Log.e(TAG, "Parse data JSON invalid " + e.getMessage());
        }
        return super.getNotification(context, intent);
    }

    @Override
    protected int getSmallIconId(Context context, Intent intent) {
        return super.getSmallIconId(context, intent);
    }
}