package nayoso.kamelioprototype.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import nayoso.kamelioprototype.activity.DetailActivity;

/**
 * Created by nikoyuwono on 3/24/15.
 */
public class CustomNotificationTouchListener extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent detailActivityIntent = new Intent(context, DetailActivity.class);
        detailActivityIntent.putExtra("url", intent.getStringExtra("url"));
        detailActivityIntent.putExtra("title", intent.getStringExtra("title"));
        detailActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(detailActivityIntent);
        Intent it = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        context.sendBroadcast(it);
    }

}
