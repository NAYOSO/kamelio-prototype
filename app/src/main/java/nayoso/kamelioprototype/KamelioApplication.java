package nayoso.kamelioprototype;

import android.app.Application;
import android.content.res.Resources;
import android.util.Log;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.SaveCallback;

/**
 * Created by nikoyuwono on 3/24/15.
 */
public class KamelioApplication extends Application {

    public KamelioApplication() {
        super();
    }

    @Override
    public void onCreate() {
        initializeParse();
    }

    private void initializeParse() {
        Resources resources = getResources();
        Parse.initialize(this, resources.getString(R.string.parse_app_id),
                resources.getString(R.string.parse_client_key));
        ParsePush.subscribeInBackground("", new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.d("PARSE", "successfully subscribed to the broadcast channel.");
                } else {
                    Log.e("PARSE", "failed to subscribe for push", e);
                }
            }
        });
        ParseInstallation parseInstallation = ParseInstallation.getCurrentInstallation();
        parseInstallation.saveInBackground();
    }
}
