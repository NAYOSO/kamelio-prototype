package nayoso.kamelioprototype.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;
import butterknife.InjectView;
import nayoso.kamelioprototype.R;
import nayoso.kamelioprototype.activity.ArticleListActivity;
import nayoso.kamelioprototype.model.ThemeModel;
import nayoso.kamelioprototype.ui.adapter.MyThemeAdapter;
import nayoso.kamelioprototype.ui.adapter.TodayThemeAdapter;

/**
 * Created by nikoyuwono on 3/20/15.
 */
public class MyThemeFragment extends Fragment {

    public static MyThemeFragment newInstance() {
        MyThemeFragment myThemeFragment = new MyThemeFragment();
        return myThemeFragment;
    }

    @InjectView(R.id.todayThemeList)
    RecyclerView todayThemeList;
    @InjectView(R.id.myThemeList)
    RecyclerView myThemeList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_theme, container, false);
        ButterKnife.inject(this, view);
        initMyThemeList();
        initTodayTheme(view);
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        clearTimerTaks(scrollerSchedule);
        clearTimers(scrollTimer);
    }

    @InjectView(R.id.todayThemeDescriptionContainer)
    RelativeLayout todayThemeDescriptionContainer;

    private void initTodayTheme(final View view) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        todayThemeList.setLayoutManager(linearLayoutManager);
        TodayThemeAdapter todayThemeAdapter = new TodayThemeAdapter(createDummyDataForTodayTheme());
        todayThemeList.setAdapter(todayThemeAdapter);
        ViewTreeObserver vto = view.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                view.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                startAutoScrolling();
            }
        });

        todayThemeDescriptionContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ArticleListActivity.class);
                getActivity().startActivity(intent);
                getActivity().overridePendingTransition(R.anim.animation_new_activity_enter,
                        R.anim.animation_new_activity_leave);
            }
        });
    }

    private void initMyThemeList() {
        myThemeList.setLayoutManager(new LinearLayoutManager(getActivity()));
        MyThemeAdapter myThemeAdapter = new MyThemeAdapter(createDummyDataForMyTheme());
        myThemeList.setAdapter(myThemeAdapter);
    }

    private static final String[] dummyTodayThemeImageUrl = {
            "http://img.retty.me/img_repo/t/01/3140027.jpg",
            "http://img.retty.me/img_repo/t/01/1673200.jpg",
            "http://img.retty.me/img_repo/t/01/924676.jpg",
            "http://img.retty.me/img_repo/t/01/191267.jpg",
            "http://img.retty.me/img_repo/t/01/456028.jpg",
            "http://img.retty.me/img_repo/t/01/932276.jpg",
            "http://img.retty.me/img_repo/t/01/2468971.jpg",
            "http://img.retty.me/img_repo/t/01/1059089.jpg"};

    private static final String[] dummyTodayThemeWebUrl = {
            "http://retty.me/area/PRE14/ARE313/SUB31301/100001187627/",
            "http://retty.me/area/PRE14/ARE31/SUB3101/100000166429/",
            "http://retty.me/area/PRE13/ARE12/SUB1201/100000004866/",
            "http://retty.me/area/PRE13/ARE12/SUB1202/100000006003/",
            "http://retty.me/area/PRE13/ARE12/SUB1204/100000001798/",
            "http://retty.me/area/PRE13/ARE2/SUB204/100000005942/",
            "http://retty.me/area/PRE13/ARE16/SUB1601/100000739421/",
            "http://retty.me/area/PRE13/ARE22/SUB2201/100000013571/"};

    private ArrayList<ThemeModel> createDummyDataForTodayTheme() {
        ArrayList<ThemeModel> themeModels = new ArrayList<>();

        int arrayLength = dummyTodayThemeImageUrl.length;
        for (int i = 0; i < arrayLength; i++) {
            ThemeModel themeModel = new ThemeModel();
            themeModel.setImageUrl(dummyTodayThemeImageUrl[i]);
            themeModel.setWebUrl(dummyTodayThemeWebUrl[i]);
            themeModels.add(themeModel);
        }

        return themeModels;
    }

    private static final String[] dummyMyThemeTitle = {
            "担々麺",
            "とんかつ",
            "ラーメン",
            "カツ丼",
            "カレー",
            "すし",
            "天丼",
            "焼き肉"
    };

    private static final String[] dummyMyThemeSubtitle = {
            "本場の味の西川担々麺　永吉",
            "目黒の名店　とんき",
            "牛骨醤油ラーメン　マタドール",
            "新潟カツ丼　タレカツ",
            "神保町の名店　ボンディ",
            "長行列築地の寿司屋　寿司大",
            "都内で美味しい香川県のうどん　オニヤンマ",
            "熟成肉が人気！　エイジング・ビーフ"
    };

    private ArrayList<ThemeModel> createDummyDataForMyTheme() {
        ArrayList<ThemeModel> themeModels = new ArrayList<>();
        int arrayLength = dummyMyThemeTitle.length;
        for (int i = 0; i < arrayLength; i++) {
            ThemeModel themeModel = new ThemeModel();
            themeModel.setImageUrl(dummyTodayThemeImageUrl[i]);
            themeModel.setTitle(dummyMyThemeTitle[i]);
            themeModel.setSubTitle(dummyMyThemeSubtitle[i]);
            themeModels.add(themeModel);
        }

        return themeModels;
    }

    private Timer scrollTimer = null;
    private TimerTask scrollerSchedule;
    private final static int SCROLL_DISTANCE = 3;

    public void startAutoScrolling() {
        if (scrollTimer == null) {
            scrollTimer = new Timer();

            if (scrollerSchedule != null) {
                scrollerSchedule.cancel();
                scrollerSchedule = null;
            }
            scrollerSchedule = new TimerTask() {
                @Override
                public void run() {
                    if (getActivity() != null) {
                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                moveScrollView();
                            }
                        });
                    }
                }
            };

            scrollTimer.schedule(scrollerSchedule, 30, 30);
        }
    }

    public void moveScrollView() {
        todayThemeList.smoothScrollBy(SCROLL_DISTANCE, 0);

    }

    private void clearTimers(Timer timer) {
        if (timer != null) {
            timer.cancel();
        }
    }

    private void clearTimerTaks(TimerTask timerTask) {
        if (timerTask != null) {
            timerTask.cancel();
        }
    }

}
