package nayoso.kamelioprototype.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import butterknife.ButterKnife;
import butterknife.InjectView;
import nayoso.kamelioprototype.R;
import nayoso.kamelioprototype.model.ThemeModel;
import nayoso.kamelioprototype.ui.adapter.RecommendedThemeAdapter;

/**
 * Created by nikoyuwono on 3/18/15.
 */
public class RecommendationFragment extends Fragment {

    public static RecommendationFragment newInstance() {
        RecommendationFragment recommendationFragment = new RecommendationFragment();
        return recommendationFragment;
    }

    @InjectView(R.id.editorPickupList)
    RecyclerView editorPickupList;
    @InjectView(R.id.newThemeList)
    RecyclerView newThemeList;
    @InjectView(R.id.rankingList)
    RecyclerView rankingList;
    @InjectView(R.id.standardList)
    RecyclerView standardList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recommendation, container, false);
        ButterKnife.inject(this, view);
        initEditorPickupList();
        initNewThemeList();
        initRankingList();
        initStandardList();
        return view;
    }

    private void initEditorPickupList() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        editorPickupList.setLayoutManager(linearLayoutManager);
        editorPickupList.setAdapter(new RecommendedThemeAdapter(createDummyData()));
    }

    private void initNewThemeList() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        newThemeList.setLayoutManager(linearLayoutManager);
        newThemeList.setAdapter(new RecommendedThemeAdapter(createDummyData()));
    }

    private void initRankingList() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rankingList.setLayoutManager(linearLayoutManager);
        rankingList.setAdapter(new RecommendedThemeAdapter(createDummyData()));
    }

    private void initStandardList() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        standardList.setLayoutManager(linearLayoutManager);
        standardList.setAdapter(new RecommendedThemeAdapter(createDummyData()));
    }

    private final static String[] wardNameList = {
            "新宿区",
            "渋谷区",
            "港区",
            "目黒区",
            "文京区",
            "品川区",
            "大田区",
            "世田谷区",
            "杉並区",
            "中野区",
            "練馬区",
            "板橋区",
            "豊島区",
            "北区",
            "千代田区",
            "中央区",
            "台東区",
            "荒川区",
            "足立区",
            "墨田区",
            "江東区",
            "江戸川区",
            "葛飾区"
    };

    private final static String[] wardPicture = {
            "http://www.mkimpo.com/image/walk/2010/04-24/IMG_5886s.jpg",
            "http://blog-imgs-44.fc2.com/k/a/r/karyuyo/hachiko.jpg",
            "http://memorva.jp/travel/tokyo/img/0011.jpg",
            "http://jun-ko.up.seesaa.net/ftp/10-4-8.jpg",
            "http://1.bp.blogspot.com/_6y3E1f09cCU/SrPhLScGRvI/AAAAAAAAAmo/neMt-RnqJTI/s400/%E8%B5%A4%E9%96%80.JPG",
            "http://www.tptc.co.jp/Portals/0/corporate/P11ph%E5%93%81%E5%B7%9D%E3%82%B3%E3%83%B3%E3%83%86%E3%83%8A%E5%9F%A0%E9%A0%AD.jpg",
            "http://img.4travel.jp/img/tcs/t/album/lrg/10/50/79/lrg_10507992.jpg",
            "http://pds.exblog.jp/pds/1/200702/15/39/f0009439_20244139.jpg",
            "http://livedoor.blogimg.jp/tobapost/imgs/9/a/9a2f88b4.JPG",
            "http://up.gc-img.net/post_img_web/2013/04/2903bb5c3d7c6ee91e9875288fce7dbe_0.jpeg",
            "http://s-ohtsuki.sakura.ne.jp/subway/toei/ooedoH14/ooedo159.jpg",
            "http://livedoor.blogimg.jp/tobapost/imgs/9/1/9134eaef.JPG",
            "http://universalestate.jp/ike622.jpg",
            "http://img.yaplog.jp/img/15/pc/m/o/m/momo-kimock/11/11062.jpg",
            "http://img.4travel.jp/img/tcs/t/album/lrg/10/05/20/lrg_10052089.jpg",
            "http://blog.imadokipc.com/archives/images/20071112_01-thumb.jpg",
            "http://livedoor.blogimg.jp/jin_segawa/imgs/0/c/0c5a39a6.jpg",
            "http://yaplog.jp/cv/shippona/img/82/toden01_1_p.jpg",
            "http://s-ohtsuki.sakura.ne.jp/subway/eidan/chiyodaH20/Sub3%20NishiNippori-KitaSenju-KitaAyase/chiyodaH2009%20184.JPG",
            "http://image1-3.tabelog.k-img.com/restaurant/images/Rvw/13045/13045573.jpg",
            "http://loca.ash.jp/photo/kttk/ku/ko/ettyujima01.jpg",
            "http://vortis.up.seesaa.net/image/060716-1.jpg",
            "http://moromorodepunk.up.n.seesaa.net/moromorodepunk/landscape/landscape_090215_001_001.jpg?d=a1"
    };

    private ArrayList<ThemeModel> createDummyData() {
        ArrayList<ThemeModel> themeModels = new ArrayList<>();
        int arrayLength = wardNameList.length;
        for (int i = 0; i < arrayLength; i++) {
            ThemeModel themeModel = new ThemeModel();
            themeModel.setImageUrl(wardPicture[i]);
            themeModel.setTitle(wardNameList[i]);
            themeModels.add(themeModel);
        }

        long seed = System.nanoTime();
        Collections.shuffle(themeModels, new Random(seed));
        return themeModels;
    }

}
