package nayoso.kamelioprototype.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.andexert.library.RippleView;

import nayoso.kamelioprototype.R;
import nayoso.kamelioprototype.utilities.KamelioUtilities;

/**
 * Created by niko on 15/03/18.
 */
public class SideMenuSection extends RippleView {

    private ImageView sectionIcon;
    private TextView sectionTitle;

    public SideMenuSection(Context context) {
        this(context, null);
    }

    public SideMenuSection(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SideMenuSection(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attrs) {
        int sectionLogoId;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            sectionLogoId = KamelioUtilities.generateViewId();
        } else {
            sectionLogoId = View.generateViewId();
        }
        sectionIcon = new ImageView(context);
        sectionIcon.setId(sectionLogoId);
        sectionTitle = new TextView(context);

        int size = (int) KamelioUtilities.convertDipToPixels(context, 30.0f);
        int margin = (int) KamelioUtilities.convertDipToPixels(context, 15.0f);

        LayoutParams logoLayoutParams = new LayoutParams(size, size);
        logoLayoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
        logoLayoutParams.setMargins(margin, 0, margin, 0);

        LayoutParams titleLayoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams
                .WRAP_CONTENT);
        titleLayoutParams.addRule(CENTER_VERTICAL);
        titleLayoutParams.addRule(RIGHT_OF, sectionLogoId);

        sectionIcon.setLayoutParams(logoLayoutParams);
        sectionTitle.setLayoutParams(titleLayoutParams);
        sectionTitle.setTextColor(context.getResources().getColor(R.color.white));
        sectionTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);


        if(attrs != null) {
            TypedArray attributeArray = context.obtainStyledAttributes(
                    attrs,
                    R.styleable.SideMenuSection);
            sectionTitle.setText(attributeArray.getString(
                    R.styleable.SideMenuSection_side_menu_title));
            sectionIcon.setImageDrawable(attributeArray.getDrawable(
                    R.styleable.SideMenuSection_side_menu_icon));
            attributeArray.recycle();
        }

        this.addView(sectionIcon);
        this.addView(sectionTitle);
    }

    public void setIcon(Drawable icon) {
        sectionIcon.setImageDrawable(icon);
    }

    public void setTitle(String title) {
        sectionTitle.setText(title);
    }

}
