package nayoso.kamelioprototype.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import nayoso.kamelioprototype.R;
import nayoso.kamelioprototype.model.ThemeModel;

/**
 * Created by nikoyuwono on 3/23/15.
 */
public class RecommendedThemeAdapter extends RecyclerView.Adapter<RecommendedThemeAdapter.RecommendedThemeViewHolder> {

    private List<ThemeModel> themeModels;
    private Context context;

    public RecommendedThemeAdapter(List<ThemeModel> themeModels) {
        this.themeModels = themeModels;
    }

    @Override
    public RecommendedThemeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recommendation_theme_item, parent, false);
        return new RecommendedThemeViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecommendedThemeViewHolder holder, int position) {
        ThemeModel themeModel = themeModels.get(position);
        Transformation transformation = new RoundedTransformationBuilder()
                .borderColor(context.getResources().getColor(R.color.transparent_kamelio_green))
                .borderWidthDp(2)
                .cornerRadiusDp(10)
                .oval(false)
                .build();

        Picasso.with(context).load(themeModel.getImageUrl()).fit().transform(transformation).into(holder.recommendedThemeThumbnail);
        holder.recommendedThemeTitle.setText(themeModel.getTitle());
    }

    @Override
    public int getItemCount() {
        return themeModels.size();
    }

    static class RecommendedThemeViewHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.recommendedThemeThumbnail)
        ImageView recommendedThemeThumbnail;
        @InjectView(R.id.recommendedThemeTitle)
        TextView recommendedThemeTitle;
        @InjectView(R.id.recommendedThemeFollow)
        TextView recommendedThemeFollow;

        public RecommendedThemeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }
    }
}
