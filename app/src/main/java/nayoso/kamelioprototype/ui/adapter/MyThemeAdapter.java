package nayoso.kamelioprototype.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import nayoso.kamelioprototype.R;
import nayoso.kamelioprototype.model.ThemeModel;

/**
 * Created by niko on 15/03/23.
 */
public class MyThemeAdapter extends RecyclerView.Adapter<MyThemeAdapter.MyThemeViewHolder> {

    private List<ThemeModel> themeModels;
    private Context context;

    public MyThemeAdapter(List<ThemeModel> themeModels) {
        this.themeModels = themeModels;
    }

    @Override
    public MyThemeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_theme_item, parent, false);
        return new MyThemeViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyThemeViewHolder holder, int position) {
        ThemeModel themeModel = themeModels.get(position);
        Transformation transformation = new RoundedTransformationBuilder()
                .borderColor(context.getResources().getColor(R.color.transparent_kamelio_green))
                .borderWidthDp(2)
                .oval(true)
                .build();

        Picasso.with(context).load(themeModel.getImageUrl()).fit().transform(transformation).into(holder.themeThumbnail);
        holder.themeTitle.setText(themeModel.getTitle());
        holder.themeSubtitle.setText(themeModel.getSubTitle());
    }

    @Override
    public int getItemCount() {
        return themeModels.size();
    }

    static class MyThemeViewHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.themeThumbnail)
        ImageView themeThumbnail;
        @InjectView(R.id.themeTitle)
        TextView themeTitle;
        @InjectView(R.id.themeSubtitle)
        TextView themeSubtitle;

        public MyThemeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }
    }
}
