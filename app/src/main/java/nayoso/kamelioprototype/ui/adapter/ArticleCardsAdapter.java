package nayoso.kamelioprototype.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import nayoso.kamelioprototype.R;
import nayoso.kamelioprototype.model.ThemeModel;

/**
 * Created by niko on 15/03/23.
 */
public class ArticleCardsAdapter extends RecyclerView.Adapter<ArticleCardsAdapter.ViewHolder> {

    private List<ThemeModel> themeModels;
    private Context context;

    public ArticleCardsAdapter(List<ThemeModel> themeModels) {
        this.themeModels = themeModels;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        context = viewGroup.getContext();
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.article_card, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        ThemeModel themeModel = themeModels.get(i);
        Picasso.with(context).load(themeModel.getImageUrl()).into(viewHolder.articleImage);
        viewHolder.articleTitle.setText(themeModel.getTitle());
    }

    @Override
    public int getItemCount() {
        return themeModels == null ? 0 : themeModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.articleImage)
        ImageView articleImage;
        @InjectView(R.id.articleTitle)
        TextView articleTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }
    }
}
