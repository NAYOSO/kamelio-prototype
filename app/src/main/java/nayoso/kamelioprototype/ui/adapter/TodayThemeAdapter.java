package nayoso.kamelioprototype.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import nayoso.kamelioprototype.R;
import nayoso.kamelioprototype.activity.DetailActivity;
import nayoso.kamelioprototype.model.ThemeModel;

/**
 * Created by nikoyuwono on 3/20/15.
 */
public class TodayThemeAdapter extends RecyclerView.Adapter<TodayThemeAdapter.TodayThemeViewHolder> {

    private List<ThemeModel> themeModels;
    private int themeModelsCount;
    private Context context;

    public TodayThemeAdapter(List<ThemeModel> themeModels) {
        this.themeModels = themeModels;
        themeModelsCount = themeModels.size();
    }

    @Override
    public TodayThemeViewHolder onCreateViewHolder(ViewGroup viewGroup, int index) {
        context = viewGroup.getContext();
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.today_theme_item, viewGroup, false);
        return new TodayThemeViewHolder(itemView, new TodayThemeOnClickListener() {
            @Override
            public void onClick(int index) {
                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra("url", themeModels.get(index).getWebUrl());
                context.startActivity(intent);
                ((Activity)context).overridePendingTransition(R.anim.animation_new_activity_enter,
                        R.anim.animation_new_activity_leave);
            }
        });
    }

    @Override
    public void onBindViewHolder(TodayThemeViewHolder todayThemeViewHolder, int index) {
        ThemeModel themeModel = themeModels.get(index % themeModelsCount);
        Picasso.with(context).load(themeModel.getImageUrl()).into(todayThemeViewHolder.thumbnail);
    }

    @Override
    public int getItemCount() {
        return Integer.MAX_VALUE;
    }

    static class TodayThemeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @InjectView(R.id.todayThemeImage)
        ImageView thumbnail;
        private TodayThemeOnClickListener todayThemeOnClickListener;

        public TodayThemeViewHolder(View itemView, TodayThemeOnClickListener todayThemeOnClickListener) {
            super(itemView);
            ButterKnife.inject(this, itemView);
            this.todayThemeOnClickListener = todayThemeOnClickListener;
            thumbnail.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Log.i("NIKO","CLICKED");
            todayThemeOnClickListener.onClick(this.getPosition());
        }
    }

    public static interface TodayThemeOnClickListener {
        public void onClick(int index);
    }
}
