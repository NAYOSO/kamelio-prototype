package nayoso.kamelioprototype.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import nayoso.kamelioprototype.R;

/**
 * Created by niko on 15/03/24.
 */
public class DetailActivity extends ActionBarActivity {

    @InjectView(R.id.webViewDetail)
    WebView webViewDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.inject(this);
        initActionBar();
        String url = getIntent().getStringExtra("url");
        webViewDetail.getSettings().setUseWideViewPort(true);
        webViewDetail.getSettings().setJavaScriptEnabled(true);
        webViewDetail.loadUrl(url);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishActivityWithAnimation();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finishActivityWithAnimation();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.toolbarTitle)
    TextView toolbarTitle;

    private void initActionBar() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }

        String title = getIntent().getStringExtra("title");

        if(title != null) {
            toolbarTitle.setText(title);
        }
    }

    private void finishActivityWithAnimation() {
        if(isTaskRoot()) {
            startActivity(new Intent(this, MainActivity.class));
        } else {
            finish();
        }
        overridePendingTransition(R.anim.animation_prev_activity_enter,
                R.anim.animation_prev_activity_leave);
    }
}
