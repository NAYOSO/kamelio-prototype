package nayoso.kamelioprototype.activity;

import android.animation.LayoutTransition;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.astuetz.PagerSlidingTabStrip;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import butterknife.ButterKnife;
import butterknife.InjectView;
import nayoso.kamelioprototype.R;
import nayoso.kamelioprototype.fragment.MyThemeFragment;
import nayoso.kamelioprototype.fragment.RecommendationFragment;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);
        initActionBar();
        initViewPager();
        initSideMenu();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        // Set animation for the searchbar
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        for (int i = 0; i < searchView.getChildCount(); i++) {
            View view = searchView.getChildAt(i);
            if (view instanceof LinearLayout) {
                LinearLayout searchBar = (LinearLayout) view;
                searchBar.setLayoutTransition(new LayoutTransition());
            }
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.mainDrawerLayout)
    DrawerLayout mainDrawerLayout;

    private void initActionBar() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,
                mainDrawerLayout, toolbar, 0, 0) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                this.syncState();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                this.syncState();
            }
        };
        actionBarDrawerToggle.syncState();
        mainDrawerLayout.setDrawerListener(actionBarDrawerToggle);
    }

    @InjectView(R.id.mainViewPager)
    ViewPager mainViewPager;
    @InjectView(R.id.mainViewPagerTab)
    PagerSlidingTabStrip mainViewPagerTab;

    private void initViewPager() {
        mainViewPager.setAdapter(new MainViewPagerAdapter(getSupportFragmentManager()));
        mainViewPagerTab.setViewPager(mainViewPager);
    }

    @InjectView(R.id.profileImage)
    ImageView profileImage;

    private void initSideMenu() {
        Transformation transformation = new RoundedTransformationBuilder()
                .borderColor(getResources().getColor(R.color.white))
                .borderWidthDp(4)
                .oval(true)
                .build();

        Picasso.with(this).load(R.drawable.profile).transform(transformation).into(profileImage);
    }

    public class MainViewPagerAdapter extends FragmentPagerAdapter {

        private final String[] PAGE_TITLES;
        private final int MYTHEME_TAB = 0;
        private final int RECOMMENDATION_TAB = 1;

        private MyThemeFragment myThemeFragment;
        private RecommendationFragment recommendationFragment;

        public MainViewPagerAdapter(FragmentManager fm) {
            super(fm);
            Resources resources = getResources();
            PAGE_TITLES = new String[]{resources.getString(R.string.main_viewpager_my_theme),
                    resources.getString(R.string.main_viewpager_recommended)};
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case MYTHEME_TAB:
                    if(myThemeFragment == null) {
                        myThemeFragment = MyThemeFragment.newInstance();
                    }
                    return myThemeFragment;
                case RECOMMENDATION_TAB:
                    if(recommendationFragment == null) {
                        recommendationFragment = RecommendationFragment.newInstance();
                    }
                    return recommendationFragment;
            }
            return null;
        }

        @Override
        public int getCount() {
            return PAGE_TITLES.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return PAGE_TITLES[position];
        }
    }
}
