package nayoso.kamelioprototype.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import nayoso.kamelioprototype.R;
import nayoso.kamelioprototype.model.ThemeModel;
import nayoso.kamelioprototype.ui.adapter.ArticleCardsAdapter;
import nayoso.kamelioprototype.ui.helper.SwipeableRecyclerViewTouchListener;

/**
 * Created by niko on 15/03/23.
 */
public class ArticleListActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_list);
        ButterKnife.inject(this);
        initActionBar();
        initList();
    }

    @Override
    public void onBackPressed() {
        finishActivityWithAnimation();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finishActivityWithAnimation();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    private void initActionBar() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
    }

    @InjectView(R.id.articleList)
    RecyclerView articleList;

    private ArrayList<ThemeModel> themeModels;
    private ArticleCardsAdapter articleCardsAdapter;

    private void initList() {
        themeModels = createDummyDataForMyTheme();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        articleList.setLayoutManager(linearLayoutManager);
        articleCardsAdapter = new ArticleCardsAdapter(themeModels);
        articleList.setAdapter(articleCardsAdapter);
        SwipeableRecyclerViewTouchListener swipeTouchListener =
                new SwipeableRecyclerViewTouchListener(articleList,
                        new SwipeableRecyclerViewTouchListener.SwipeListener() {
                            @Override
                            public boolean canSwipe(int position) {
                                return true;
                            }

                            @Override
                            public void onDismissedBySwipeLeft(RecyclerView recyclerView, int[] reverseSortedPositions) {
                                for (int position : reverseSortedPositions) {
                                    Toast.makeText(ArticleListActivity.this, themeModels.get(position).getTitle() + "はテーマと無関係のでこの記事を報告しました。", Toast.LENGTH_SHORT).show();
                                    themeModels.remove(position);
                                    articleCardsAdapter.notifyItemRemoved(position);
                                }
                                articleCardsAdapter.notifyDataSetChanged();
                            }

                            @Override
                            public void onDismissedBySwipeRight(RecyclerView recyclerView, int[] reverseSortedPositions) {
                                for (int position : reverseSortedPositions) {
                                    Toast.makeText(ArticleListActivity.this, themeModels.get(position).getTitle() + "をクリップしました。", Toast.LENGTH_SHORT).show();
                                    themeModels.remove(position);
                                    articleCardsAdapter.notifyItemRemoved(position);
                                }
                                articleCardsAdapter.notifyDataSetChanged();
                            }
                        });

        articleList.addOnItemTouchListener(swipeTouchListener);
    }

    private void finishActivityWithAnimation() {
        finish();
        overridePendingTransition(R.anim.animation_prev_activity_enter,
                R.anim.animation_prev_activity_leave);
    }

    private static final String[] dummyTodayThemeImageUrl = {
            "http://www.maff.go.jp/kanto/chiikinet/tokyo/photo/img/taitouku_sakura.jpg",
            "http://tabisuke.arukikata.co.jp/mouth/26170/image",
            "http://blogimg.goo.ne.jp/user_image/38/ee/d19dffb202bedb6836384256f1104040.jpg",
            "http://blog-imgs-47.fc2.com/u/r/b/urbanreallife/shi0075.jpg",
            "http://www7a.biglobe.ne.jp/~tokyo-sanpo/3d/zenpukuji60.jpg",
            "http://cdn.mkimg.carview.co.jp/minkara/spot/000/000/646/131/646131/646131.jpg?ct=70733db63e86",
            "http://static-tripplanner.s3.amazonaws.com/image/upload/plan/c7c56fa2de225940cf134ba1aa5ce9bd67c4e4b8/main_large.jpg",
            "http://ratio.sakura.ne.jp/uploads/2009/04/imgp3950.jpg"};

    private static final String[] dummyMyThemeTitle = {
            "上野公園",
            "昭和記念公園",
            "井の頭公園",
            "浜離宮恩賜庭園",
            "善福寺公園",
            "芝公園",
            "代々木公園",
            "新宿御苑"
    };

    private static final String[] dummyMyThemeSubtitle = {
            "本場の味の西川担々麺　永吉",
            "目黒の名店　とんき",
            "牛骨醤油ラーメン　マタドール",
            "新潟カツ丼　タレカツ",
            "神保町の名店　ボンディ",
            "長行列築地の寿司屋　寿司大",
            "都内で美味しい香川県のうどん　オニヤンマ",
            "熟成肉が人気！　エイジング・ビーフ"
    };

    private ArrayList<ThemeModel> createDummyDataForMyTheme() {
        ArrayList<ThemeModel> themeModels = new ArrayList<>();
        int arrayLength = dummyMyThemeTitle.length;
        for (int i = 0; i < arrayLength; i++) {
            ThemeModel themeModel = new ThemeModel();
            themeModel.setImageUrl(dummyTodayThemeImageUrl[i]);
            themeModel.setTitle(dummyMyThemeTitle[i]);
            themeModel.setSubTitle(dummyMyThemeSubtitle[i]);
            themeModels.add(themeModel);
        }

        return themeModels;
    }
}
